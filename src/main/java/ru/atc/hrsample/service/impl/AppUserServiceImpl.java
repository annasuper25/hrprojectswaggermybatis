package ru.atc.hrsample.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import ru.atc.hrsample.entity.Role;
import ru.atc.hrsample.entity.User;
import ru.atc.hrsample.dao.RoleMapper;
import ru.atc.hrsample.dao.UserMapper;
import ru.atc.hrsample.service.api.AppUserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class AppUserServiceImpl implements AppUserService, UserDetailsService {

    private final UserMapper userMapper;
    private final RoleMapper roleMapper;


    @Override
    public List<User> getAllUsers() {
        return userMapper.getAllUsers();
    }

    @Override
    public User getUserById(int userId) {
        return userMapper.getUserById(userId);
    }

    @Override
    public void addUser(User user) {
        userMapper.addUser(user);
    }

    @Override
    public void updateUser(User user) {
        userMapper.updateUser(user);
    }

    @Override
    public void deleteUser(int userId) {
        userMapper.deleteUser(userId);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user =  userMapper.getUserByName(username)
                .orElseThrow(() -> new UsernameNotFoundException(String.format("Username %s not found", username)));
        Set<Role> roles = roleMapper.getRolesByUserid(user.getUserId());
        user.setRoles(roles);
        return user;
    }
}
