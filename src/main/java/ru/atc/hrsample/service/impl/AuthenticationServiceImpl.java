package ru.atc.hrsample.service.impl;

import lombok.extern.slf4j.Slf4j;
import ru.atc.hrsample.auth.LoginRequestDTO;
import ru.atc.hrsample.auth.RegistrationRequestDTO;
import ru.atc.hrsample.entity.Role;
import ru.atc.hrsample.entity.User;
import ru.atc.hrsample.dao.RoleMapper;
import ru.atc.hrsample.dao.UserMapper;
import ru.atc.hrsample.exceptionHandling.UserDuplicateException;
import ru.atc.hrsample.service.api.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;


@Service
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;
    private AuthenticationManager authenticationManager;
    private RoleMapper roleMapper;

    @Autowired
    public AuthenticationServiceImpl(UserMapper userMapper, PasswordEncoder passwordEncoder, AuthenticationManager authenticationManager, RoleMapper roleMapper) {
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
        this.authenticationManager = authenticationManager;
        this.roleMapper = roleMapper;
    }

    @Override
    @Transactional
    public Authentication authorize(LoginRequestDTO loginRequestDTO) {
        UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                new UsernamePasswordAuthenticationToken(loginRequestDTO.getLogin(), loginRequestDTO.getPassword());

        Authentication authentication = authenticationManager.authenticate(usernamePasswordAuthenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return authentication;
    }

    @Override
    public User register(RegistrationRequestDTO registrationRequestDTO) {
        userMapper.getByLogin(registrationRequestDTO.getUsername())
                .ifPresent((account)-> {
                    throw new UserDuplicateException("Such user already exists");
                });
        log.error("Пользователь %s уже существует", registrationRequestDTO.getUsername());
        return createAccount(registrationRequestDTO);
    }

    @Override
    public void logout() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }


    private User createAccount(RegistrationRequestDTO registrationRequestDTO) {
        User account = new User();
        Set<Role> roles = new HashSet<>();
        int roleId = registrationRequestDTO.getRoleId();

        account.setPassword(passwordEncoder.encode(registrationRequestDTO.getPassword()));
        account.setUsername(registrationRequestDTO.getUsername());

        userMapper.addUser(account);
        userMapper.addRoleToUser(account.getUserId(),roleId);

        Role role = roleMapper.getRoleById(registrationRequestDTO.getRoleId());
        roles.add(role);
        account.setRoles(roles);
        return account;
    }

}
