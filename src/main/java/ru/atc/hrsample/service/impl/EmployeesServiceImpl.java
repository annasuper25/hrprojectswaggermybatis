package ru.atc.hrsample.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.atc.hrsample.dao.DepartmentsMapper;
import ru.atc.hrsample.dao.EmployeesMapper;
import ru.atc.hrsample.dao.JobsMapper;
import ru.atc.hrsample.dto.*;
import ru.atc.hrsample.entity.DepartmentsEntity;
import ru.atc.hrsample.entity.EmployeesEntity;
import ru.atc.hrsample.entity.JobsEntity;
import ru.atc.hrsample.service.api.EmployeesService;


import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;


/**
 * Реализация сервиса по работе с работниками
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class EmployeesServiceImpl implements EmployeesService {

    private final ModelMapper modelMapper;
    private final EmployeesMapper employeesMapper;
    private final JobsMapper jobsMapper;
    private final DepartmentsMapper departmentsMapper;

    @Override
    @Transactional(readOnly = true)
    public EmployeesDto getEmployeeById(Integer id) {
        EmployeesEntity employeesEntity = employeesMapper.getEmployeeById(id).orElseThrow(
                () -> new RuntimeException(String.format("Не найдена сущность работника по идентификатору=%s", id))
        );
        log.debug("Получена сущность работника с id={}", id);

        EmployeesDto employeesDto = modelMapper.map(employeesEntity, EmployeesDto.class);

        //Добавляем в объект информацию о должности:
        jobsMapper.getJobById(employeesEntity.getJobId()).ifPresent(jobEntity -> employeesDto.setJobs(jobEntity));


        //Добавляем в объект информацию о менеджере:
        Integer idManager = employeesEntity.getManagerId();
        employeesMapper.getEmployeeById(idManager).ifPresent(managerEntity -> employeesDto.setManager(managerEntity));


        //Добавляем в объект информацию о департаменте:
        Integer idDepartment = employeesEntity.getDepartmentId();
        departmentsMapper.getDepartmentById(idDepartment).ifPresent(depEntity -> employeesDto.setDepartment(depEntity));

        return employeesDto;
    }


    @Override
    @Transactional
    public EmployeesDto insertEmployee(EmployeesRequestDto employeesRequestDto) throws RuntimeException {
        EmployeesEntity employeesEntity = modelMapper.map(employeesRequestDto, EmployeesEntity.class);

        JobsDto jobsDto = employeesRequestDto.getJobs();
        JobsEntity jobsEntityFromDto = modelMapper.map(jobsDto, JobsEntity.class);

        if (employeesRequestDto.getSalary() > jobsDto.getMaxSalary()
                || employeesRequestDto.getSalary() < jobsDto.getMinSalary()) {

            throw new RuntimeException(
                    String.format("Сумма зарплаты не соответствует заданным для работы с id = %s ограничениям", jobsDto.getJobId())
            );
        }


        //Если передан идентификатор работы
        if (!(jobsDto.getJobId().isEmpty() || jobsDto.getJobId().equals("0") || jobsDto.getJobId().equals("string"))) {

            List<JobsEntity> jobsEntities = jobsMapper.getAll();

            boolean isExistJobId = false;
            for (JobsEntity entity : jobsEntities
                    ) {
                if (entity.getJobId().equals(jobsDto.getJobId())) {
                    isExistJobId = true;
                }
            }

            //проверка существует ли такой JobId в БД
            if (!isExistJobId) {

                jobsMapper.insert(jobsEntityFromDto);
            }

            employeesEntity.setJobId(jobsDto.getJobId());
        } else {
            throw new RuntimeException(String.format("Необходимо заполнить идентификатор работы нового сотрудника (поле jobId)")
            );
        }

        employeesMapper.insert(employeesEntity);
        log.info("Создана сущность работника с id={}", employeesEntity.getEmployeeId());

        EmployeesDto employeesDto = getEmployeeById(employeesEntity.getEmployeeId());

        return employeesDto;
    }


    @Override
    @Transactional(readOnly = true)
    public List<EmployeesDto> getAll() {
        List<EmployeesEntity> employeesEntities = employeesMapper.getAll();
        log.debug("Получены все сущности работников");

        return modelMapper.map(employeesEntities, TypeToken.of(List.class).getType());
    }

    @Override
    public List<EmployeesDto> getAllWithFilter(String firstName, String lastName, String email, String startDate, String endDate) {

        List<EmployeesEntity> employeesEntities = employeesMapper.getAll();
        log.debug("Получены все сущности работников");

        //Фильтруем список сотрудников, если введен фильтр по имени
        if (!StringUtils.isEmpty(firstName)) {
            Predicate<EmployeesEntity> FirstNameIsNotEquals = employee -> !employee.getFirstName().equalsIgnoreCase(firstName);
            employeesEntities.removeIf(FirstNameIsNotEquals);
            log.debug("Сущности работников отфильтрованы по имени");
        }

        //Фильтруем список сотрудников, если введен фильтр по фамилии
        if (!StringUtils.isEmpty(lastName)) {
            Predicate<EmployeesEntity> LastNameIsNotEquals = employee -> !employee.getLastName().equalsIgnoreCase(lastName);
            employeesEntities.removeIf(LastNameIsNotEquals);
            log.debug("Сущности работников отфильтрованы по фамилии");
        }

        //Фильтруем список сотрудников, если введен фильтр по е-мэйл
        if (!StringUtils.isEmpty(email)) {
            Predicate<EmployeesEntity> emailIsNotEquals = employee -> !employee.getEmail().equalsIgnoreCase(email);
            employeesEntities.removeIf(emailIsNotEquals);
            log.debug("Сущности работников отфильтрованы по е-мэйл");
        }

        //Фильтруем список сотрудников, если введен фильтр по дате с
        if (!StringUtils.isEmpty(startDate)) {
            LocalDate localStartDate = LocalDate.parse(startDate);

            Predicate<EmployeesEntity> HireDateIsNotAfterStartDate = employee -> !employee.getHireDate().isAfter(localStartDate);
            employeesEntities.removeIf(HireDateIsNotAfterStartDate);
            log.debug("Сущности работников отфильтрованы по дате устройства с...");
        }

        //Фильтруем список сотрудников, если введен фильтр по дате по
        if (!StringUtils.isEmpty(endDate)) {
            LocalDate localEndDate = LocalDate.parse(endDate);

            Predicate<EmployeesEntity> HireDateIsNotBeforeEndDate = employee -> !employee.getHireDate().isBefore(localEndDate);
            employeesEntities.removeIf(HireDateIsNotBeforeEndDate);
            log.debug("Сущности работников отфильтрованы по дате устройства до...");
        }

        return modelMapper.map(employeesEntities, TypeToken.of(List.class).getType());
    }
}
