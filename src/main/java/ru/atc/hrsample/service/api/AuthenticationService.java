package ru.atc.hrsample.service.api;


import ru.atc.hrsample.auth.LoginRequestDTO;
import ru.atc.hrsample.auth.RegistrationRequestDTO;
import ru.atc.hrsample.entity.User;
import org.springframework.security.core.Authentication;

public interface AuthenticationService {

    Authentication authorize(LoginRequestDTO loginRequestDTO);

    User register(RegistrationRequestDTO registrationRequestDTO);

    void logout();

}
