package ru.atc.hrsample.controller;

import lombok.RequiredArgsConstructor;
import ru.atc.hrsample.auth.LoginRequestDTO;
import ru.atc.hrsample.auth.RegistrationRequestDTO;
import ru.atc.hrsample.entity.User;
import ru.atc.hrsample.exceptionHandling.UserDuplicateEntity;
import ru.atc.hrsample.exceptionHandling.UserDuplicateException;
import ru.atc.hrsample.service.api.AuthenticationService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;


/**
 * REST контроллер для работы с пользователями
 */

@Tag(name = "Пользователи", description = "API для учетных записей польователей ")
@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class UserController {

    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public Authentication login(@RequestBody @Validated LoginRequestDTO loginRequestDTO) {
        return authenticationService.authorize(loginRequestDTO);
    }

    @PostMapping("/logout")
    public void logout() {
        authenticationService.logout();
    }


    @PostMapping("/register")
    public User register(@RequestBody @Validated RegistrationRequestDTO registrationRequestDTO) {
        return authenticationService.register(registrationRequestDTO);
    }

    @PostMapping("/current")
    public Principal current(Principal principal) {
        return principal;
    }


    @ExceptionHandler
    public ResponseEntity<UserDuplicateEntity> handleException (UserDuplicateException exception) {
        UserDuplicateEntity user = new UserDuplicateEntity();
        user.setInfo(exception.getMessage());
        return new ResponseEntity<>(user, HttpStatus.NOT_ACCEPTABLE);
    }
}
