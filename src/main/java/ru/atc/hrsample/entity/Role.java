package ru.atc.hrsample.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Role implements GrantedAuthority {

    private Integer roleId;
    private String roleName;

    @Override
    public String getAuthority() {
        return roleName;
    }
}
