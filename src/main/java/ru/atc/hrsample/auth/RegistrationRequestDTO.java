package ru.atc.hrsample.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * Модель запроса на регистрацию
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(title = "Модель данных запроса на регистрацию")
public class RegistrationRequestDTO {

    @NotNull
    private String username;

    @NotNull
    private String password;

    private Integer roleId;

}
