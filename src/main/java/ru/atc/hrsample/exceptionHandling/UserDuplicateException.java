package ru.atc.hrsample.exceptionHandling;

public class UserDuplicateException extends RuntimeException {

    public UserDuplicateException (String message) {
        super(message);
    }
}
