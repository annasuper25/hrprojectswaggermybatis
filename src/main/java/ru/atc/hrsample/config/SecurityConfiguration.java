package ru.atc.hrsample.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ru.atc.hrsample.service.impl.AppUserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

  //  private final PasswordEncoder passwordEncoder;
    private final AppUserServiceImpl userService;

    @Bean
    public PasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();}

    @Autowired
    public SecurityConfiguration( AppUserServiceImpl userService) {
      //  this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/v3/api-docs/**", "/swagger-ui.html", "/swagger-ui/**");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/api/login").permitAll()
                .antMatchers(HttpMethod.POST, "/api/register").permitAll()
                .antMatchers(HttpMethod.GET).hasAnyRole("EMPLOYEE", "MANAGER", "ADMIN")
                .antMatchers(HttpMethod.POST, "/locations").hasAnyRole("ADMIN", "MANAGER")
                .antMatchers(HttpMethod.POST, "/departments").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, "/employees").hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT, "/locations").hasAnyRole("ADMIN", "MANAGER")
                .antMatchers(HttpMethod.PUT, "/departments").hasAnyRole("ADMIN", "MANAGER")
                .antMatchers(HttpMethod.PUT, "/employees").hasAnyRole("ADMIN", "MANAGER")
                .antMatchers(HttpMethod.DELETE).hasRole("ADMIN");
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }


    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder, PasswordEncoder passwordEncoder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(userService)
                .passwordEncoder(passwordEncoder);
    }


}
