insert into roles VALUES ( nextval('role_id_seq'), 'ROLE_EMPLOYEE'),
                         ( nextval('role_id_seq'), 'ROLE_MANAGER'),
                         ( nextval('role_id_seq'), 'ROLE_ADMIN');

insert into users values
(nextval('user_id_seq'), 'admin', '$2y$12$f/HnrvmP6ov5MPDWXm2R.OgI5YRXKnuig9Y99hN.PMuO8p43kRrVe'),
(nextval('user_id_seq'), 'dan', '$2y$12$FCRxysNeUY.mWoMGcqn62emH503Yft3hAtJkeN0KeELfNIIajlMQ2'),
(nextval('user_id_seq'), 'mrsblack', '$2y$12$uzwx/8zkFydbhGuWkwjytuH5A3HWBjzrVY5P0tfI35ApNeDhniZmq');

insert into users_roles values
(1, 3),
(2, 1),
(3, 2);


insert into locations (street_address, city) values
('pleshka 1000', 'Moskva'),
('pleshka 9999', 'Vladivostok'),
('pleshka 1000', 'TEST'),
('pleshka 1000', 'SPB'),
('pleshka 1000', 'Perm');

insert into jobs  VALUES
('job1', 'job1', 1000, 2000),
('job2', 'job2',  2222, 4000);

INSERT INTO employees (first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id)
VALUES ('Ivan', 'Ivanov', 'ivan_ivanov@mail.ru', '79551112233', '2018-01-07', 'job1', 1100, NULL, NULL);

INSERT INTO departments (department_name, manager_id, location_id)
VALUES ('Main department', 1, 1);

INSERT INTO employees (first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id)
VALUES ('Petr', 'Petrov', 'petr_petrov@mail.ru', '79551112200', '2017-01-07', 'job1', 1200, 1, 1),
('ALina', 'Smirnova', 'alina_smirnova@mail.ru', '79651762780', '2020-01-01', 'job2', 4000, 2, 1);


UPDATE employees
SET department_id = 1  WHERE employee_id = 1;

INSERT INTO departments (department_name, manager_id, location_id)
VALUES ('Moscow department 1', 2, 2),
('Tula department', 3, 5);

insert into employees (first_name, last_name, email, phone_number, hire_date, job_id, salary, manager_id, department_id) VALUES
('Evgeniy', 'Borisov', 'email2@mail.ru', '56897874', '2020-01-01', 'job2', 1000, 1, 1),
('Ivan', 'Ivanov', 'email3@mail.ru', '56568989', '2020-01-01', 'job2', 4000, 2, 3),
('Evgeniy', 'Ivanov', 'email24@mail.ru', '56111874', '2003-01-06', 'job2', 3000, 2, 3);



INSERT INTO job_history
VALUES (1, '11 03 2018', '16 06 2021', 'job1', 1),
(2, '10 09 2017', '16 06 2019', 'job2', 1),
(2, '17 06 2019', '19 03 2021', 'job1', 1),
(3, '19 09 2014', '16 06 2016', 'job1', 3),
(3, '17 06 2016', '16 06 2021', 'job2', 2);
